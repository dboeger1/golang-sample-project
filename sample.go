package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

/*
This sample golang function simply generates random unsigned integers between 1
and 100, inclusive. The bulk of the actual logic is in the corresponding test
file, which will generate varied PASS/FAIL results for the purpose of evaluating
CI/CD tools for test automation.
*/
func GenerateResult() uint {
	// Seed the random number generator.
	rand.Seed(time.Now().UTC().UnixNano())

	// Return a random result from 1 to 100, inclusive.
	return (uint)(rand.Intn(100) + 1)
}

/*
This main function allows a result to be generated on the command line for
development purposes. However, the test file will call the above generation
function directly.
*/
func main() {
	// Generate and print a result.
	var result uint = GenerateResult()
	fmt.Printf("Result: %v\n", result)

	// Exit with the result as the exit code.
	os.Exit((int) (result))
}
