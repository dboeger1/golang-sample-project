package main

import (
	"fmt"
	"testing"
)

// Declare global constants.
const MINIMUM_FAILURE_PROBABILITY uint =   0
const MAXIMUM_FAILURE_PROBABILITY uint = 100

/*
This top-level test method simulates a variety of tests on golang project code
with varying probabilities of failure. This is useful for generating varied
results for the purpose of evaluating CI/CD tools for test automation.

Realistically, most typical golang projects would contain many different
features and functions that would need to be individually tested with their own
corresponding test fucntions, but for the sake of this simulation, all tests
will be testing a single sample golang function. This sample function generates
random results which are unsigned integers between 1 and 100, inclusive. The
tests then produce PASS/FAIL output based on how those randomly generated
results compare to the specified failure probability. In this way, the
simulation produces test results which exercise the test reporting functionality
of CI/CD pipelines.
*/
func TestFailureProbabilities(test *testing.T) {
	// Iterate through all the possible failure probabilities.
	var failureProbability uint;
	for
		failureProbability = MINIMUM_FAILURE_PROBABILITY;
		failureProbability <= MAXIMUM_FAILURE_PROBABILITY;
		failureProbability++ {
		// Run a separate test for each individual failure probability.
		test.Run(
			fmt.Sprintf(
				"FailureProbability=%v%%",
				failureProbability,
			),
			func(test *testing.T) {
				testFailureProbability(test, failureProbability)
			},
		)
	}
}

/*
This is the underlying test function containing the actual test logic which
produces PASS/FAIL results according to the specified failure probability and
the randomly generated result of the sample function under test.
*/
func testFailureProbability(test *testing.T, failureProbability uint) {
	// Restrict the failure probability to the valid window of values.
	if failureProbability < MINIMUM_FAILURE_PROBABILITY {
		failureProbability = MINIMUM_FAILURE_PROBABILITY
	}
	if failureProbability > MAXIMUM_FAILURE_PROBABILITY {
		failureProbability = MAXIMUM_FAILURE_PROBABILITY
	}

	// Generate a result from 1 to 100, inclusive.
	var result uint = GenerateResult()

	// Log the PASS/FAIL result.
	if result <= failureProbability {
		test.Fatalf(
			"FAIL! Result (%v) <= failure probability (%v).\n",
			result,
			failureProbability,
		)
	} else {
		test.Logf(
			"PASS! Result (%v) > failure probability (%v).\n",
			result,
			failureProbability,
		)
	}
}
